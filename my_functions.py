def main():
    response = greeting("John")
    print(response)


def greeting(name: str) -> str:
    return f"Hello {name}"

if __name__ == "__main__":
    main()